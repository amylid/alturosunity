﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Alturos.Yolo;
using Alturos.Yolo.Model;

public class phoneCamera : MonoBehaviour {
    private bool camAvailable;
    private Texture defaultBackground;
    private int c;
    private WebCamDevice[] devices;
    private WebCamTexture backCam;

    public RawImage background;
    public AspectRatioFitter fit;

    private YoloWrapper yolo;

    // Start is called before the first frame update
    private void Start () {
        //defaultBackground = background.texture;
        devices = WebCamTexture.devices;

        if (devices.Length == 0) {
            Debug.Log ("No camera detected");
            camAvailable = false;
            return;
        }

        Debug.Log(devices.Length + " cameras detected");
        for (int i = 0; i < devices.Length; i++)
            Debug.Log (devices[i].name);

        c = 0;

        /* Check for cameras

        for (int i = 0; i < devices.Length; i++) {
            if (!devices[i].isFrontFacing) {
                backCam = new WebCamTexture (devices[i].name, Screen.width, Screen.height);
            }
        }
        if (backCam == null) {
            Debug.Log ("No back-camera detected");
            camAvailable = false;
            return;
        }
        */
        
        backCam = new WebCamTexture (devices[0].name, Screen.width, Screen.height);
        backCam.Play ();
        background.texture = backCam;
        camAvailable = true;
        
        var configurationDetector = new ConfigurationDetector(); 
        var config = configurationDetector.Detect("Assets/YoloData");
        Debug.Log(config.ConfigFile + "\n" + config.WeightsFile + "\n" + config.NamesFile);
        yolo = new YoloWrapper(config);

    }

    // Update is called once per frame
    void Update () {
        if (!camAvailable)
            return;

        //calculate aspect ratio
        float ratio = (float) backCam.width / (float) backCam.height;
        fit.aspectRatio = ratio;

        //flip image if mirrored
        float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;
        background.rectTransform.localScale = new Vector3 (1f, scaleY, 1f);

        //rotate screen with device
        int orient = -backCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3 (0, 0, orient);

        //fix portrait mode aspect ratio
        if (orient == -90 || orient == -270) 
            background.rectTransform.localScale = new Vector3 (ratio, ratio, 1f);

        /*
        Detect();
    }

    private void Detect()
    {*/
        Texture2D rawImageTexture = background.texture as Texture2D; 
        byte[] pngData = rawImageTexture.EncodeToPNG();
        var items = yolo.Detect(pngData);
        //AddDetailsToPictureBox(pictureBox1, items);
    }

    public void ChangeCam () { //change camera
        if (!camAvailable)
            return;

        c++;
        Debug.Log ("c: " + c + " " + devices[c % devices.Length].name);

        backCam = new WebCamTexture (devices[c % devices.Length].name, Screen.width, Screen.height);
        backCam.Play ();
        background.texture = backCam;
    }
}